# chrome.system.memory Plugin

This plugin provides the ability to get physical memory information.

## Status

Stable on Android and iOS.

## Caveats

### Android

The total amount of physical memory capacity is available only for Jelly Bean and after.

## Reference

The API reference is [here](https://developer.chrome.com/apps/system_memory).
